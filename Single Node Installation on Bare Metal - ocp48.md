### Important - Things to ensure before installation
-----------------------------------------------------
1. **DHCP**: If there's DHCP in the installation environment and you're intending to use it, notice:
  
    The DHCP configuration must refer to the bare-metal server MAC address and offers the right IP. Use MAC reservations. There's no need for bootstrap VM.
2. **DNS**: ensure the DNS server in use has the environment zone has following records for control-plane, api, api-int, etcd, etcd-server-ssl and apps, all with server's IP.
3. **Network**:
    1. Ensure the right FW rules are open for ports 22(ssh), 6443(api), 443(apps), 53(dns)
    2. Ensure there's reasonable connection between the server and the registry
4. **Installer**: Ensure you have an installer vm with **rhel8** as its operating system. The vm should have connection to the server over ports 22, 6443 and 443.
5. **Registry**: Ensure all the images of the version you're about to install are in the registry

Prepare the environment
-----------------------
**The Installer VM**
1. Create or login to your installer vm. Verify its OS is **rhel8**:
```
cat /etc/os-release
```
The first 2 lines in the output should be `NAME="Red Hat Enterprise Linux"/ VERSION="8*"`. Also verify that the **coreos-installer** is usable:
```
coreos-installer
```
2. Create or verify an installation directory with the name of the cluster you're about to create.
3. Get the `openshift-install.tar.gz` that matches the openshift version you want to install. Extract it, make it executable and copy it to one of the PATH paths (so it will be easier to use):
```
tar -exf openshift-install.tar.gz
chmod u+x openshift-install
cp openshift-install /usr/local/bin
```
4. Get the `rhcos-<cluster version>-x86_64-live.iso` image and place it in the installation directory.
5. For reinstalling - Remove the configs directory to ensure no old ignition file or hidden log file left from previous installation is still present:
```
rm -rf <install-dir>/configs
```
6. Create a sub directory in the installation directory for the ignition file:
```
mkdir <install-dir>/configs
```
7. Create and customize the install-config.yaml file, especially where the <fill in> shows:
```
apiVersion: v1
baseDomain: <fill in> 
compute:
- architecture: amd64
  hyperthreading: Enabled   
  name: worker
  replicas: 0
  platform: {}
controlPlane:
  architecture: amd64
  hyperthreading: Enabled   
  name: master
  replicas: 1
  platform: {}
metadata:
  name: <fill in>
networking:
  clusterNetwork:
  - cidr: 10.128.0.0/14 
    hostPrefix: 23 
  networkType: OpenShiftSDN
  serviceNetwork: 
  - 172.30.0.0/16 
platform:
  none: {}
pullSecret: '{"auths": <fill in>}' 
sshKey: 'ssh-ed25519 AAAA<fill in>'
additionalTrustBundle: | 
  -----BEGIN CERTIFICATE-----
  <fill in>
  -----END CERTIFICATE----- 
imageContentSources: 
- mirrors:
  - <local_registry>/<local_repository_name>/release
  source: quay.io/openshift-release-dev/ocp-release
- mirrors:
  - <local_registry>/<local_repository_name>/release
  source: quay.io/openshift-release-dev/ocp-v4.0-art-dev
  ```
8. Backup the file for next installations:
```
cp install-config.yaml install-config.yaml.bkp
```
9. copy the file to configs sub-directory:
```
cp install-config.yaml configs/
```

**The bare-metal server**
1. Login to the management UI of the server (ILO, CIMC, etc.)
2. Launch KVM > html-based
3. In the opened window, choose Virtual media > activate virtual media > map cd/dvd > apply the clean live iso - *without the ignition file embed*
4. Boot and press F2 to reach the BIOS menu
5. In the BIOS menu, go to Boot Order and verify that the RAID or your bootable disk is the first option and that the kvm is the second
6. Go to the last page of the menu and choose save and reset option
7. For reinstalling:

  **Note: If after few reboots you encounter an issue where the disk names changes, make sure you are booting from **legacy** mode and not **UEFI**. If the issue continue, consider upgrading the firmware of the server**
  
  you must wipe the bootable disk (usually RAID1 disk). To do so, you have 2 options:
  1. Wipe the disk by deleting and re-creating the RAID. It doesn't always work, especially if there's LVM on the disk:
    * In the BIOS menu, go to Advanced
    * Look for the bigger RAID and press Enter
    * Go to Configure and delete the existing virtual drive
    * Recreate the RAID (create new virtual drive) with RAID1
    * Save and boot
  2. Boot with clean live iso (without the ignition file embed). Once its booted, run   `lsblk`.
  If there are LVM partitions, you need to remove these from the disk:
  ```
  lvremove <lv name>
  vgremove <vg name>
  pvremove <pv name>
  ```
  After running these commands, delete every partition on the boot disk.

Installation
-------------
1. In the installer VM, make sure you are located in the installation directory.
2. Generate manifests with the following command:
```
openshift-install --dir=/<install-dir>/configs create manifests
```
3. In non-DHCP environment, you will have to configure manifest for the network configuration:

    1. Create and customize the machine config file named `99_openshift-machineconfig_99-master-network.bu`:
      ```
      variant: openshift
      version: 4.8.0
      metadata:
        name: 99-master-custom 
        labels:
          machineconfiguration.openshift.io/role: master
      openshift:
        kernel_arguments:
          - loglevel=7
      storage:
        files:
          - path: /etc/sysconfig/network-scripts/ifcfg-ens192
            mode: 0644
            overwrite: true
            contents:
              inline: |
                TYPE=Ethernet
                NAME="ens192"
                DEVICE="ens192"
                ONBOOT=yes
                NETBOOT=yes
                BOOTPROTO=none
                IPADDR=""
                NETMASK="255.255.255.0"
                GATEWAY=""
                DNS1=""
            - path: /etc/hostname
              mode: 0644
              overwrite: true
              contents:
                inline: |
                  <hostname>.<cluster name>.<base domain>
      ```
      2. Convert the bu file to yaml:
     ```
     butane 99_openshift-machineconfig_99-master-network.bu -o 99_ openshift-machineconfig_99-master-network.yaml
      ```
      3. Copy the yaml file to its detsination:
      ```
      cp 99_openshift-machineconfig_99-master-network.yaml <install-dir>/configs/openshift/
      ```
  
4. Generate the ignition file with the following command:
```
openshift-install --dir=<install-dir>/configs create single-node-ignition-config
```
5. In non-DHCP environment, you have to add network configuration to the ignition file:
      1. Open 2 ssh sessions to your installer VM
      2. In one session, run the command and save its output:
            ```
            echo 'TYPE=Ethernet
            NAME="ens192"
            DEVICE="ens192"
            ONBOOT=yes
            NETBOOT=yes
            BOOTPROTO=none
            IPADDR=""
            NETMASK="255.255.255.0"
            GATEWAY=""
            DNS1=""' | base64 -w0
            ```

      3. In the other session, open the bootstrap-in-place-for-live-iso.ign:
            ```
            vim configs/bootstrap-in-place-for-live-iso.ign
            ```

      Run `:%!jq .` so the next step will be easier. Find {"storage": "files":{}} and add the following:

            ```
            {
              "overwrite": true,
              "path": "/etc/sysconfig/network-scripts/ifcfg-ens192",
              "user": {
                "name": "root"
              },
              "contents": {
                "source": "data:text/plain;charset=utf-8;base64,<output of step 5.2>"
              },
              "mode": 420
            },
            ```

      Save the file but don't close it for next steps:

        ```
        esc
        :w+Enter
        ```
      
      4. In the session where you run step 5.2, run the following and save the output:
        ```
        echo '<hostname>.<cluster name>.<base domain>' | base64 -w0
        ```
      
      5. In the session where bootstrap-in-place-for-live-iso.ign is open, add the following under what you've added in step 5.3:
      ```
      {
        "overwrite": true,
        "path": "/etc/hostname",
        "user": {
          "name": "root"
        },
        "contents": {
          "source": "data:text/plain;charset=utf-8;base64,<output of step 5.4>"
        },
        "mode": 420
      },
      ```
      Escape the 'Insert' mode (simply esc) and return back to the original layout of the file:
      ```
      :%!jq -c .
      ```
      Save and exit.
  
6. Run the following command to embed the ignition file with the live iso. Make sure to backup the clean iso and save it in safe place **before** you run this command. 
```
coreos-installer iso ignition embed -fi bootstrap-in-place-for-live-iso rhcos-<cluster version>-x86_64-live.iso
```
If you want to inspect what's inside the iso, run:
```
coreos-installer iso ignition show ../rhcos-<cluster version>-x86_64-live.iso
```
7. Download the embed iso to your local station
8. In the server KVM , shut it down if it's not powered off.
9. Choose Virtual media > activate virtual media > map cd/dvd > add the embed iso
10. Turn on the server and press F6 until the boot order appears
11. Choose kvm-mapped-cd/dvd and press Enter
12. Let the server boot with the embed iso. It might reboot after the first boot, but shouldn't reboot more than once or two.

There you go! You should now have a single-node openshift cluster on your bare metal server!

Validate installation
---------------------
If you want to follow the installation and ensure everything is working properly, you can run these commands:
#### On the installer
`openshift-install --dir=/<install-dir>/configs wait-for install-complete --log-level=debug`
#### On the control plane
`journalctl -b -f -u release-image -u bootkube`
`journalctl -b -f -u install-to-disk`
`sudo crictl ps -a`
`sudo podman images`

Written by
----------
_Reishit Kosef_
